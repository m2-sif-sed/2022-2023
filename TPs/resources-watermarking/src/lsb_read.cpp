#include "image.h"
#include "mtrand.h"

int main(int argc, char** argv)
{
	if(argc!=3)
	{
		cout << argv[0] << " src.ppm dst.pgm\n"; 
		cout << "\t src.ppm  : image marquée\n"; 
		cout << "\t dst.pgm  : image lue\n\n"; 
		
		return -1; 
	}
	
	mtsrand(1);
	
	image<rgb> 		src; 
	image<octet>	dst;
	
	src.read(argv[1]); 

	dst.resize(src.width(), src.height()); 
	
	
	
	// To do
	
	
	
	dst.write(argv[2]); 
	
	return 1; 
}