Goal: Given a dataset synthetically generated ("synth.csv") and a set of targets ("targets.csv"), guess which target was part of the dataset used to train the generator. Half the targets were in the training dataset, half were out.

Algorithm: The algorithm used for generating the synthetic data is the winner of the 2018 NIST challenge (see paper 2 attached). The epsilon parameter was set to 1000 (i.e., the perturbation has probably a negligible impact).

Data: Information on the original dataset can be found here : https://microdata.epi.org/variables/. You can also inspect the original dataset and play with the synthetic data generator by cloning the "cli" branch of the Snake repository: https://github.com/snake-challenge/snake1/tree/cli. 

Baseline attack: The code given in the Synthetic Data Release repository (https://github.com/spring-epfl/synthetic_data_release) can be used as a baseline. It is described in paper 1 attached.

Deadline: Please send me your guess in the file called "attack.txt" such that line i is set to 1 if you think that target i is in the training data (set to 0 otherwise). The lines of the "attack.txt" file that you received are set to 0 or 1 uniformly at random.

Contact: Do not hesitate to contact tristan.allard@irisa.fr if you need additional information.